#include <iostream>
#include <vector>
#include <time.h>


int partitionAndSort(std::vector<int> &v, int start, int end);
/*
    1. Pick a pivot point
    2. Partition and sort the array
    3. Sort left side recursively
    4. Sort right side recursively
*/ 

// qs([5, 3, 9, 1, 4], 0, 4) First call
//      start = 0, end = 4
//      pivot = 2 (from partitionAndSort)
//      qs([3, 1, 4, 5, 9], 0, 1) Left side
//          start = 0, end = 1
//          pivot = 0 (from partitionAndSort)
//          qs([1, 3, 4, 5, 9] , 0, -1) start is not smaller than end, nothing happens here.
//          qs([1, 3, 4, 5, 9] , 1, 1) start is is not smaller than end, nothing happens here
//      qs([1, 3, 4, 5, 9], 3, 4) rightside
//          start = 3, end = 4
//          pivot = 4 (from partitionAndSort)
//          qs([1, 3, 4, 5, 9], 3, 2) start is not smaller than end, nothing happens here.
//          qs([1, 3, 4, 5, 9], 4, 4) start is not smaller than end, nothing happens here.
// All done, v = [1, 3, 4, 5, 9]

void quickSort(std::vector<int> &v, int start, int end)
{
    using namespace std;

    if(start < end)
    {
        size_t pivot = partitionAndSort(v, start, end);
        quickSort(v, start, pivot - 1); // Sort left side of pivot point
        quickSort(v, pivot + 1, end); // Sort right side of pivot point
    }
}

/*
    Re-arrange so that all values smaller than pivot points are to the left of pivot point and
    all values larger than pivot point are to the right of the pivot point.
*/

/*
    *** Enter partitionAndSort ***
    start = 0, end  = 4    
    v = [5, 3, 9, 1, 4]
    pivot = 4
    indLess = -1

    loop
         X
    v = [5, 3, 9, 1, 4] do nothing
            X
    v = [5, 3, 9, 1, 4]  3 < 4
    indLess = 0
    swap v[1] and v[0]
               X
    v = [3, 5, 9, 1, 4] do nothing
                  X
    v = [3, 5, 9, 1, 4]  1 < 4
    indLess = 1
    swap v[3] and v[1]
    v = [3, 1, 9, 5, 4] 
    loop end

    swap v[4] and v[2]
    v = [3, 1, 4, 5, 9]
    return 2
    *** Exit partitionAndSort ***


    *** Enter partitionAndSort ***
    start = 0, end = 1
    v = [3, 1, 4, 5, 9]
    pivot = 1
    indLess = -1
          
    loop
         X
    v = [3, 1, 4, 5, 9] nothing happens
    end loop

    swap v[0] and v[1]
    v = [1, 3, 4, 5, 9] 
    return 0
    *** Exit partitionAndSort ***

    *** Enter partitionAndSort ***
    start = 3, end = 4
    v = [1, 3, 4, 5, 9]
    pivot = 9
    indless = 2

    loop
                  X
    v = [1, 3, 4, 5, 9] 5 < 9 
    swap v[3] and v[3] -> nothing happens
    v = [1, 3, 4, 5, 9]
    indless = 3

    end loop

    swap v[4] and v[4] -> nothing happens
    v = [1, 3, 4, 5, 9]
    return 4
    *** Exit partitionAndSort ***
*/    
int partitionAndSort(std::vector<int> &v, int start, int end)
{
    int pivot = v[end]; // Choose the last element as pivot point. For this implemenation, it is crucial that
                        // the pivot point we choose is the LAST element. If you switch to say pivot = v[start] and
                        // leave the rest of this code unchanged, it will no longer work.

    int indLess = start - 1; // A reference to an index that we will move to the right as we sort the vector
    for(int i = start; i < end; i++)
    {        
        // Current element less than pivot, should be to the left of the pivot point
        if(v[i] < pivot)
        {
            indLess++; // move sliding window to the right
            // Swap v[indLess] and v[i]
            int tmp = v[indLess];
            v[indLess] = v[i];
            v[i] = tmp;
        }
    }

    // Swap indLess + 1 and last element
    int tmp = v[(indLess + 1)];
    v[(indLess + 1)] = v[end];
    v[end] = tmp;

    return (indLess + 1);
}

/*
    Driver code
*/
int main()
{
    using namespace std;
    
    vector<int> array = {5, 3, 9, 1, 4};
    quickSort(array, 0, array.size() - 1);
    for(auto v : array)
    {
        cout << v << ", ";
    }
    cout << endl;

    // Sort some random arrays
    cout << "**************" << endl;
    srand(time(NULL));
    for(int i = 0; i < 5; i++)
    {
        array.clear();
        int lengthRand = 5 + rand() % 4;
        for(int j  = 0 ; j < lengthRand; j++)
        {
            int rndValue = -5 + rand() % 15;
            array.push_back(rndValue);
        }

        cout << "Unsorted: " ;
        for(auto v : array)
        {
            cout << v << ", ";
        }
        cout << endl;

        quickSort(array, 0, array.size() - 1);

        cout << "Sorted: " ;
        for(auto v : array)
        {
            cout << v << ", ";
        }
        cout << endl;
    }

    return 0;
}